package com.nyancraft.module.formatchannel;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.feildmaster.channelchat.channel.Channel;
import com.feildmaster.channelchat.channel.ChannelManager;
import com.feildmaster.channelchat.event.player.ChannelPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Formatter implements Listener {
    private final Formaty plugin;
    private String format;

    public Formatter(Formaty plugin){
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event){
        Channel channel = (event instanceof ChannelPlayerChatEvent) ? ((ChannelPlayerChatEvent)event).getChannel() : ChannelManager.getManager().getActiveChannel(event.getPlayer());
        if(channel == null) return;
        Player player = event.getPlayer();
        /*** Channels ***/
        System.out.println(channel.getName());
        System.out.println(plugin.channels.get(channel.getName()));
        if(plugin.universalFormat){
            format = plugin.supportedPluginsFound ? plugin.defaultFormat.replace("%player", "%s").replace("%message", "%s").replace("%prefix", getPrefix(player)).replace("%suffix", getSuffix(player)) : plugin.defaultFormat.replace("%player", "%s").replace("%message", "%s").replace("%prefix", "").replace("%suffix", "");
        }else{
            if(plugin.channels.get(channel.getName()) != null){//if(plugin.channels.containsValue(channel.getName())){
                format = plugin.supportedPluginsFound ? plugin.channels.get(channel.getName()).replace("%player", "%s").replace("%message", "%s").replace("%prefix", getPrefix(player)).replace("%suffix", getSuffix(player)) : plugin.channels.get(channel.getName()).replace("%player", "%s").replace("%message", "%s").replace("%prefix", "").replace("%suffix", "");
            }else{
                format = plugin.supportedPluginsFound ? plugin.defaultFormat.replace("%player", "%s").replace("%message", "%s").replace("%prefix", getPrefix(player)).replace("%suffix", getSuffix(player)) : plugin.defaultFormat.replace("%player", "%s").replace("%message", "%s").replace("%prefix", "").replace("%suffix", "");
            }
        }
        /*** End Channels ***/

        String colourized = ChatColor.translateAlternateColorCodes('&', format);

        event.setFormat(colourized);
    }
    private String getPrefix(Player player){
        return Formaty.chat.getPlayerPrefix(player);
    }
    private String getSuffix(Player player){
        return Formaty.chat.getPlayerSuffix(player);
    }	
}
