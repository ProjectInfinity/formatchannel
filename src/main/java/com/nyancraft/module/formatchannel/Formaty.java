package com.nyancraft.module.formatchannel;

import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.feildmaster.channelchat.event.channel.ReloadEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;

import com.feildmaster.channelchat.Module;

public class Formaty extends Module{
    private static Formaty plugin;
    private final Logger log = Logger.getLogger("Minecraft");

    public String defaultFormat;
    public static Chat chat = null;
    public static Permission permission = null;
    public boolean supportedPluginsFound = true;
    public boolean universalFormat;
    public Map<String, String> channels = new HashMap<String, String>();

    public void onDisable() {}

    public void onEnable() {
        if(getServer().getPluginManager().getPlugin("Vault") != null){
            if (!setupChat() || !setupPermissions()) {
                log.info("[FormatChannel] No supported plugins found! Prefix and suffixes will not work.");
                supportedPluginsFound = false;
            }
        }else{
            supportedPluginsFound = false;
        }

        plugin = this;
        reloadConfig();
        getServer().getPluginManager().registerEvents(new Formatter(this), plugin);
    }

    @EventHandler
    public void onReload(ReloadEvent event){
        reloadConfig();
    }

    private void reloadSettings() {
       defaultFormat = getConfig().getString("defaultFormat");
       universalFormat = getConfig().getBoolean("universalFormat");
        for(String channel : getConfig().getConfigurationSection("Channels").getKeys(false)){
            System.out.println(channel);
            channels.put(channel, getConfig().getString("Channels." + channel, defaultFormat));
            System.out.println(getConfig().getString("Channels." + channel));
        }
    }

    public void reloadConfig() {
        super.reloadConfig();
        if(getConfig().needsUpdate()) {
            saveDefaultConfig();
        }
        reloadSettings();
    }
    public static Formaty getPlugin() {
        return plugin;
    }
    private boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        return (chat != null);
    }
    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }
}